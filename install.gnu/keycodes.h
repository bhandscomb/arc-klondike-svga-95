/* keycodes.h */

#define BACKSPACE    0x008
#define ENTER        0x00d
#define ESCAPE       0x01b
#define F10          0x144
#define HOME         0x147
#define CURSOR_UP    0x148
#define CURSOR_LEFT  0x14b
#define CURSOR_RIGHT 0x14d
#define END          0x14f
#define CURSOR_DOWN  0x150
#define INSERT       0x152
#define DELETE       0x153
