The source code is provided for those of you who program and are curious as
to how I've gone about things. I make no claims to writing the most efficient
code, or that it will deal with all hardware and software, or even that the
code is totally free of bugs. My only claim is that it has worked on every
machine I've tried it on, and runs at an acceptable speed on my machine. For
reference, my machine is based on a 100 MHz 486 DX4. Additional testing has
been performed on a 120 MHz Pentium, the video card for which forced me to
implement slightly more advanced VESA code a little earlier than expected. My
thanks to a good friend for this additional testing.

Many things have been done in certain ways because I don't know any better,
and I will admit that my previous PC programming was text based with a small
amount of EGA work - big jump, huh??

