Klondike 95     BETA README
~~~~~~~~~~~

This version of Klondike was inspired by Klondike Deluxe AGA for the Amiga,
and in fact uses the same cardsets.

REQUIREMENTS

386 processor or above *
VESA 1.2+ compatible Super VGA graphics card (min 1Mb)
128K conventional memory
1.5Mb DPMI memory **
1.4 to 3Mb hard disk space (depending on options)
Windows 95 not essential, but v.nice - long filenames are supported!

*  Though the game as a whole has only been tested on 486 and Pentium based
   machines, the cardset handling is known to work on 386 machines.

** DPMI services are required - e.g. Qemm (QDPMI) or (hint hint) Windows 95.
