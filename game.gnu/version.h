/* version.h */

/* version info */
#define VERNUMB      "1.1p"
#define VERDATE      "27.04.97"

/* release type - BETATEST or REALGAME */
#define RELEASETYPE  BETATEST
