/* files.h */

/* directories */
#define DECKDIR        "DECKS\\"
#define DATADIR        "DATA\\"

/* picture filenames */
#define TITLEPICNAME   DATADIR "TITLE.KLN"
#define WINPICNAME     DATADIR "WIN.KLN"

/* music filenames */
#define INTROMUSNAME   DATADIR "INTRO.MPX"
#define GAMEMUSNAME    DATADIR "GAME.MPX"
#define WINMUSNAME     DATADIR "WIN.MPX"
#define INTROMODNAME   DATADIR "INTRO.MOD"
#define GAMEMODNAME    DATADIR "GAME.MOD"
#define WINMODNAME     DATADIR "WIN.MOD"

/* decks */
#define DEFAULTDECK            "DEFAULT.REK"
#define PREFSDECK      DATADIR "STDPREFS.REK"

/* config */
#define CONFIGFILENAME DATADIR "KLONDIKE.CFG"
