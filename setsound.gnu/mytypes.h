/* mytypes.h */

typedef long           LONG;
typedef short          WORD;
typedef char           BYTE;

typedef unsigned long  ULONG;
typedef unsigned short UWORD;
typedef unsigned char  UBYTE;